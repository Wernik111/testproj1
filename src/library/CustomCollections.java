package library;

import library.unmodifiable.UnmodifiableCollection;
import library.unmodifiable.UnmodifiableList;
import library.unmodifiable.UnmodifiableRandomAccessList;

import java.util.Collection;
import java.util.List;
import java.util.RandomAccess;

public class CustomCollections {
	@SuppressWarnings("unchecked")
	public static <T> Collection<T> unmodifiableCollection(Collection<? extends T> c) {
		if (c.getClass() == UnmodifiableCollection.class) {
			return (Collection<T>) c;
		}
		return new UnmodifiableCollection<>(c);
	}
	@SuppressWarnings("unchecked")
	public static <T> List<T> unmodifiableList(List<? extends T> list) {
		if (list.getClass() == UnmodifiableList.class || list.getClass() == UnmodifiableRandomAccessList.class) {
			return (List<T>) list;
		}

		return (list instanceof RandomAccess ?
				new UnmodifiableRandomAccessList<>(list) :
				new UnmodifiableList<>(list));
	}
}
