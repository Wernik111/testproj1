package library.unmodifiable;

import java.util.List;
import java.util.RandomAccess;

public class UnmodifiableRandomAccessList<E> extends UnmodifiableList<E>
		implements RandomAccess
{
	public UnmodifiableRandomAccessList(List<? extends E> list) {
		super(list);
	}

	public List<E> subList(int fromIndex, int toIndex) {
		return new UnmodifiableRandomAccessList<>(
				list.subList(fromIndex, toIndex));
	}
}