import library.CustomCollections;

import java.util.ArrayList;
import java.util.List;

public class Main {
	public static void main(String[] args) {
		List<Integer> integers = new ArrayList<>();
		integers.add(2);
		integers.add(4);
		System.out.println(integers);

		// Раскомментировать для исключения, связанного с невозможностью
		// добавить в неизменяемую коллекцию элементы.

		/*integers = CustomCollections.unmodifiableList(integers);
		integers.add(5);
		System.out.println(integers);*/
	}
}